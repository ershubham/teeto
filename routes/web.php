<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','MainController@mainPage')->name('welcome');
Route::get('/halls','MainController@hallBooking')->name('hallBooking');
Route::get('/halls/search','MainController@search')->name('search');
Route::get('/halls/booking/{id}/{from_date}/{to_date}','MainController@bookingform')->name('bookingform');
Route::post('/halls/do_booking','MainController@do_booking')->name('do_booking');
Route::get('/shop','ShoppingController@mainPage')->name('mainPage');
Route::get('/shop/addtocart/{id}','ShoppingController@addtocart')->name('shop.addtocart');
Route::get('/shop/delfromcart/{id}','ShoppingController@delfromcart')->name('shop.delfromcart');
Route::get('/shop/cart','ShoppingController@cart')->name('shop.cart');


Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth', 'admin']], function () {
    // user routes
        Route::group(['prefix'=>'admin'], function () {
          Route::get('/dashboard','AdminController@dashboard')->name('dashboard');
          Route::get('/hall_request','AdminController@hall_request')->name('hall_request');
          Route::get('/add_hall','AdminController@add_hall')->name('add_hall');
          Route::get('/view_halls','AdminController@view_halls')->name('view_halls');
          Route::post('/save_hall','AdminController@save_hall')->name('save_hall');
          Route::get('/add_product', [
          'uses' =>'AdminController@add_product',
          'as' =>'shop.add_product'
        ]);
          Route::post('/save_product', [
          'uses' =>'AdminController@save_product',
          'as' =>'shop.save_product'
        ]);
          Route::get('/manage_product', [
          'uses' =>'AdminController@manage_product',
          'as' =>'shop.manage_product'
        ]);
        Route::get('/orders', [
        'uses' =>'AdminController@all_orders',
        'as' =>'admin.orders'
      ]);
      Route::get('/delPro/{id}', [
      'uses' =>'AdminController@delPro',
      'as' =>'admin.delPro'
    ]);
        Route::get('/order_details/{id}', [
          'uses' =>'AdminController@order_details',
          'as' =>'user.my_orders'
      ]);
        Route::post('/updateorder', [
          'uses' =>'AdminController@updateorderstatus',
          'as' =>'user.updateorder'
      ]);
        });
        Route::group(['prefix'=>'user'], function () {
          Route::get('/checkout','UserController@checkout')->name('checkout');
          Route::post('/save_address', [
              'uses' =>'UserController@save_address',
              'as' =>'user.save_address'
          ]);
          Route::post('/generateOrder', [
              'uses' =>'UserController@generateOrder',
              'as' =>'user.generateOrder'
          ]);
          Route::get('/orderconfirmation', [
              'uses' =>'UserController@orderconfirmation',
              'as' =>'user.orderconfirmation'
          ]);
          Route::get('/my_orders', [
              'uses' =>'UserController@my_orders',
              'as' =>'user.my_orders'
          ]);
          Route::get('/my_account', [
              'uses' =>'UserController@my_account',
              'as' =>'user.my_account'
          ]);
          Route::post('/save_edit_profile', [
              'uses' =>'UserController@save_edit_profile',
              'as' =>'user.save_edit_profile'
          ]);

          Route::get('/order_details/{id}', [
              'uses' =>'UserController@order_details',
              'as' =>'user.my_orders'
          ]);
        });
});
