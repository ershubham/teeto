<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\product;
use DB;
use Cookie;
use Session;
use App\temp_cart;

class ShoppingController extends Controller
{
    //
    public function mainPage(Request $request)
    {
      $sid = $request->cookie('tempkey');
      // $locid = $request->cookie('locid');
    $Allpro=product::all();

      if($sid == null){
          $token = 0;
          do{
          $token = uniqid();
          $check = \DB::table('temp_carts')->where('cookies_key',$token)->first();
        }while(count($check) != 0);
          $minutes = 3600;
          $response = new Response(view('shop.mainPage')->with('products',$Allpro));
          $response->withCookie(cookie('tempkey', $token , $minutes));
          return $response;
      }
      else {

        return view('shop.mainPage')->with('products',$Allpro);
      }

    }
    public function addtocart($id,Request $request)
    {
      # code...
      $tempkey=$request->cookie('tempkey');
      // echo $tempkey;
      // exit;
      $pro= DB::table('products')->where('id', '=', $id)->first();
      $prointemp = DB::table('temp_carts')->where([
            ['cookies_key', '=', $tempkey],
            ['pid', '=', $pro->id],
          ])->first();
      if ($prointemp===null) {
          //not in cart insert
            $tempIns=new temp_cart();
          $tempIns->cookies_key=$tempkey;
          $tempIns->pid=$pro->id;
          $tempIns->qty=1;
          $tempIns->price=$pro->sell_price;
          $tempIns->save();
      } else {
          // updte the cart
            $tempID=$prointemp->id;
          $upTemp=temp_cart::find($tempID);
          $upTemp->qty=$prointemp->qty+1;
          $upTemp->price=$prointemp->price+$pro->sell_price;
          $upTemp->save();
      }
      return \Redirect::back()->with('msg', 'product added successfully');
    }
    public function cart(Request $request)
    {
        $tempkey=$request->cookie('tempkey');
      # code...
      $cart_item = DB::table('temp_carts')
          ->join('products', 'temp_carts.pid', '=', 'products.id')
          ->select('temp_carts.*', 'products.*')
          ->where('cookies_key', $tempkey)
          ->get();

      return view('shop/cart')->with('cartitem', $cart_item);
    }
    public function delfromcart($id,Request $request)
    {
        # code...
      $tempkey=$request->cookie('tempkey');
        $pro= DB::table('products')->where('id', '=', $id)->first();
        $prointemp = DB::table('temp_carts')->where([
          ['cookies_key', '=', $tempkey],
          ['pid', '=', $pro->id],
          ])->first();
        if ($prointemp->qty>1) {
            $tempID=$prointemp->id;
            $upTemp=temp_cart::find($tempID);
            $upTemp->qty=$prointemp->qty-1;
            $upTemp->price=$prointemp->price-$pro->sell_price;
            $upTemp->save();
        } else {
            $tempID=$prointemp->id;
            $upTemp=temp_cart::find($tempID);
            $upTemp->delete();
        }
        return \Redirect::back()->with('msg', 'Cart Updated successfully');
    }
    
}
