<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hall;
use App\HallQuery;
class MainController extends Controller
{
    //
    public function mainPage()
    {
      # code...
      return view('main_page');
    }
    public function hallBooking()
    {
      # code...
      return view('halls.main_page');
    }
    public function search(Request $request)
    {
      # code...
      $from_date=$request['from_date'];
      $to_date=$request['to_date'];
      $capacity=$request['capacity'];
      $getHalls=Hall::where('capacity','=',$capacity)->get();
      return view('halls.search_page')->with('halls',$getHalls);
    }
    public function bookingform($id,$from_date,$to_date)
    {
      # code...
      $getHallsDetails=Hall::find($id);
      return view('halls.bookingform')->with('halls',$getHallsDetails)->with('from_date',$from_date)->with('to_date',$to_date);
    }
    public function do_booking(Request $request)
    {
      # code...
      $hall_id=$request['hall_id'];
      $from_date=$request['from_date'];
      $to_date=$request['to_date'];
      $cust_name=$request['cust_name'];
      $cust_email=$request['cust_email'];
      $cust_contact=$request['cust_contact'];
      $cust_address=$request['address'];
      $save_query=new HallQuery();
      $save_query->hall_id=$hall_id;
      $save_query->from_date=$from_date;
      $save_query->to_date=$to_date;
      $save_query->user_full_name=$cust_name;
      $save_query->user_email=$cust_email;
      $save_query->user_contact_no=$cust_contact;
      $save_query->user_address=$cust_address;
      $save_query->save();
      return view('halls.bookingsuccess')->with('requestID',$save_query->id);
    }

}
