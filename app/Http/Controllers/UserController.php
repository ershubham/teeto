<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\shopping_address;
use App\order;
use App\order_detail;
class UserController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function checkout(Request $request)
    {
      # code...
        $tempkey=$request->cookie('tempkey');
      $cart_item = DB::table('temp_carts')
            ->join('products', 'temp_carts.pid', '=', 'products.id')
            ->select('temp_carts.*', 'products.*')
            ->where('cookies_key', $tempkey)
            ->get();

        $shipping_address=DB::table('shopping_addresses')->where('user_id', \Auth::User()->id)->get();
        return view('shop/checkout')->with('ship', $shipping_address)->with('cartitem', $cart_item);
    }
    public function save_address(Request $request)
    {
        # code...
          $tempkey=$request->cookie('tempkey');
      $saveShip=new shopping_address();
        $saveShip->user_id=\Auth::User()->id;
        $saveShip->contact_name=$request['ship_name'];
        $saveShip->contact_number=$request['con_number'];
        $saveShip->address=$request['address'];
        $saveShip->city=$request['city'];
        $saveShip->state=$request['state'];
        $saveShip->landmark=$request['landmark'];
        $saveShip->pincode=$request['pincode'];
        $saveShip->save();
        return redirect('user/checkout');
    }
    public function generateOrder(Request $request)
    {
        # code..
          $tempkey=$request->cookie('tempkey');
      $shipping_id=$request['address'];
        $total_amount=$request['total_amount'];
        $payment_mode=$request['payment'];
        $od_ins= new order();
        $od_ins->user_id=\Auth::User()->id;
        $od_ins->total_amount=$total_amount;
        $od_ins->total_qnty="0";
        $od_ins->shipping_mode=$shipping_id;
        $od_ins->payment_mode=$payment_mode;
        $od_ins->payment_status="pending";
        $od_ins->order_status="pending";
        $od_ins->order_details="Order Initiated";
        $od_ins->save();
        $order_id=$od_ins->id;

        \Session::put('order_id', $order_id);
      //get from cart and insert
      $cart_item = DB::table('temp_carts')
            ->where('cookies_key', $tempkey)
            ->get();

        foreach ($cart_item as $cart) {
            $ods= new order_detail();
            $ods->order_id=$order_id;
            $ods->product_id=$cart->pid;
            $ods->qty=$cart->qty;
            $ods->cost=$cart->price;
            $ods->shipping_charges="0";
            $ods->save();
        }
        DB::table('temp_carts')->where('cookies_key', $tempkey)->delete();
        if ($payment_mode=="online") {

        } else {
            # code...
        return redirect('user/orderconfirmation')->with('order_id', $order_id)->with('status', 'successfully');
        }
    }

    public function orderconfirmation()
    {
        return view('shop.orderconfirmartion');
    }
    public function my_orders()
    {
        $id=\Auth::User()->id;
        $orderlist=DB::table('orders')->where('user_id', $id)->orderBy('id', 'desc')->get();
        return view('shop/my_orders')->with('orderlist', $orderlist);
    }
    public function order_details($id)
    {
        $user_id=\Auth::User()->id;
        $order_basic=DB::table('orders')->where('user_id', \Auth::User()->id)->where('id', $id)->first();
      // $order_details=DB::table('order_details')->where('order_id',$id)->get();
      $order_details=DB::table('order_details')
            ->join('products', 'order_details.product_id', '=', 'products.id')
            ->select('order_details.*', 'products.*')
            ->where('order_id', $id)
            ->get();
        if (count($order_basic)) {
            $shipid=$order_basic->shipping_mode;
            $shipping_details=DB::table('shopping_addresses')->where('id', $shipid)->first();
            return view('shop/order_details')->with('order', $order_basic)->with('order_details', $order_details)->with('ship', $shipping_details);
        } else {
            return redirect('user/404');
        }
    }
    public function my_account()
    {
        $userdata=DB::table('users')->where('id', \Auth::User()->id)->first();
        return view('shop/my_account')->with('userdata', $userdata);
    }
    public function save_edit_profile(Request $request)
    {
        $crUser=User::find(\Auth::User()->id);
        $crUser->name=$request['name'];
        $crUser->save();
        return redirect('shop/my_account');
    }

}
