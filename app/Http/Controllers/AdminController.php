<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\HallQuery;
use App\Hall;
use App\product;
use App\order;
use DB;
class AdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function dashboard()
    {
      return view('admin.dashboard');

    }
    public function hall_request()
    {
      # code...
      $allRe=HallQuery::all();
      return view('admin.hall_request')->with('hall_request',$allRe);
    }
    public function add_hall()
    {
      # code...
      return view('admin.add_hall');
    }
    public function save_hall(Request $request)
    {
      # code...
      $save_hall=new Hall();
      $save_hall->name=$request['hall_name'];
      $save_hall->capacity=$request['capacity'];
      $save_hall->venue=$request['venue'];
      $save_hall->image="halls.jpeg";
      $save_hall->suit_for=$request['suitable'];
      $save_hall->price_range=$request['pricerange'];
      $save_hall->save();
      return redirect('admin/view_halls');
    }
    public function view_halls()
    {
      # code...
      $allHall=Hall::all();
      return view('admin.view_halls')->with('hall',$allHall);
    }
    public function add_product()
    {

        return view('admin/add_product');
    }

    public function save_product(Request $request)
    {
        $destinationPath = 'public'; // upload path
        $extension = Input::file('p_image')->getClientOriginalExtension(); // getting image extension
        $p_image = rand(11111, 99999) . '.' . $extension; // renameing image
        Input::file('p_image')->move($destinationPath, $p_image);
        $save_pro = new product();
        $save_pro->name = $request['p_name'];
        $save_pro->description = $request['p_des'];
        $save_pro->image = $p_image;
        $save_pro->sell_price = $request['p_selling'];
        $save_pro->save();
        return redirect('admin/add_product')->with('msg', 'successfully added product');
    }

    public function manage_product()
    {
        # code...
        $pro = DB::table('products')->get();
        return view('admin/manage_product')->with('pro', $pro);
    }
    public function all_orders()
    {
      $orderlist=DB::table('orders')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->select('orders.*', 'users.*')
            ->orderBy('orders.id', 'desc')->get();
      // $orderlist=DB::table('orders')->orderBy('id', 'desc')->get();
      return view('admin/orders')->with('orderlist',$orderlist);
    }
   public function delPro($id)
    {
        $delPr = product::find($id);
        $delPr->delete();
        return redirect('admin/manage_product')->with('msg', 'product status changed');
    }
    public function order_details($id)
    {
      $order_basic=DB::table('orders')->where('id',$id)->first();
      // $order_details=DB::table('order_details')->where('order_id',$id)->get();
      $order_details=DB::table('order_details')
            ->join('products', 'order_details.product_id', '=', 'products.id')
            ->select('order_details.*', 'products.*')
            ->where('order_id',$id)
            ->get();
        $shipid=$order_basic->shipping_mode;
        $shipping_details=DB::table('shopping_addresses')->where('id',$shipid)->first();
        return view('admin/order_details')->with('order',$order_basic)->with('order_details',$order_details)->with('ship',$shipping_details);
    }
    public function updateorderstatus(Request $request)
    {
      $od=$request['order_id'];
      $ups=order::find($od);
      $ups->order_status=$request['status'];
      $ups->order_details=$request['des'];
      $ups->save();
      return \Redirect::back()->with('msg','Status Updated successfully');
    }
}
