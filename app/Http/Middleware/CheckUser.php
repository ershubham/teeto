<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckUser
{
    /**
    * Handle an incoming request.
        *
        * @param  \Illuminate\Http\Request  $request
        * @param  \Closure  $next
        * @return mixed
        */
    public function handle($request, Closure $next)
    {
        if (Auth::user()) {
            $user = Auth::user();
            if ($request->is('admin/*') && $user->user_type !== 'admin') {
                return redirect('/login');
            }
            if ($request->is('user/*') && $user->user_type !== 'user') {
                return redirect('/shop/login');
            }
        }
        return $next($request);
    }
}
