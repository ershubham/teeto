@include('halls.header')
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-success">
        <div class="panel-heading">
          <h3 class="panel-title">Request Your Booking</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-lg-4">
              <form class="" action="/halls/do_booking" method="post">
              {{csrf_field()}}
              <div class="form-group">
                <label for="">Full Name</label>
                <input type="text" name="cust_name" class="form-control" id="" placeholder="">
                <p class="help-block"></p>
              </div>
              <div class="form-group">
                <label for="">Email</label>
                <input type="email" name="cust_email" class="form-control" id="" placeholder="">
                <p class="help-block"></p>
              </div>
              <div class="form-group">
                <label for="">Contact No</label>
                <input type="number" name="cust_contact" class="form-control" id="" placeholder="">
                <p class="help-block"></p>
              </div>
              <div class="form-group">
                <label for="">Address</label>
                <textarea name="address" class="form-control" rows="8" cols="40"></textarea>
                <p class="help-block"></p>
              </div>
              <input type="hidden" name="hall_id" value="{{$halls->id}}">
              <input type="hidden" name="from_date" value="{{$from_date}}">
              <input type="hidden" name="to_date" value="{{$to_date}}">
              <div class="form-group">
                <label for=""></label>
                <input type="submit" class="form-control btn btn-primary form-control" id="" placeholder="">
                <!-- <p class="help-block">Help text here.</p> -->
              </div>
              </form>
            </div>
            <div class="col-lg-4 col-lg-offset-4">
              <h3>Hall Details</h3>
              <div class="panel panel-default">
                <div class="panel-body">
                  <img src="/{{$halls->image}}" style="width:200px" alt="" />
                </div>
                <div class="panel-footer">
                  {{$halls->name}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('halls.footer')
