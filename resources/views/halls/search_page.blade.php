@include('halls.header')
<div class="container">
<div class="row">
  <div class="col-lg-12">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h3 class="panel-title">Best search for your need</h3>
    </div>
    <div class="panel-body">
      @foreach($halls as $hs)
        <div class="row">
          <div class="col-lg-3">
            <img src="/{{$hs->image}}" alt="" style="width:150px; height:150px" />
          </div>
          <div class="col-lg-6">
            <h3>{{$hs->name}}</h3>
            <p>
              <ul>
                <li><span class="label label-success">Venue</span> : {{$hs->venue}}</li>
                <li><span class="label label-success">Suitable for </span> : {{$hs->suit_for}}</li>
                <li><span class="label label-success">Capacity </span> : {{$hs->capacity}} person</li>
              </ul>
            </p>
          </div>
          <div class="col-lg-3" style="padding-top:20px">
            <h4 class="text-center">Price Starting From </br>{{$hs->price_range}}</h4>
            <a href="/halls/booking/{{$hs->id}}/{{$_GET['from_date']}}/{{$_GET['to_date']}}" class="btn btn-primary form-control">Book Hall</a>
          </div>
        </div>
      @endforeach
    </div>
  </div>
  </div>
</div>
</div>
@include('halls.footer')
