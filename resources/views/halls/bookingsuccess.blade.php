@include('halls.header')
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="text-center">Thanks for your query</h1>
      <h3 class="text-center">We have recorded your query with refernce no #{{$requestID}}</h3>
    </div>
  </div>
</div>
@include('halls.footer')
