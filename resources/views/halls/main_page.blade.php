@include('halls.header')
<section id="banner">
  <div class="container searchBox">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="text-center">
          Welcome to HallHUBs
        </h1>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 well" style="color:black">
        <form class="" action="/halls/search" method="get">
          <div class="form-group col-lg-3">
            <label for="">From Date</label>
            <input type="date" name="from_date" class="form-control" id="" placeholder="">
            <p class="help-block">Booking from date</p>
          </div>
          <div class="form-group col-lg-3">
            <label for="">To Date</label>
            <input type="date" name="to_date" class="form-control" id="" placeholder="">
            <p class="help-block">Booking from date</p>
          </div>
          <div class="form-group col-lg-3">
            <label for="">Capacity</label>
              <select class="form-control" name="capacity">
                <option value="">Select Capacity</option>
                <option value="0-50">0-50 person</option>
                <option value="51-100">51-100 person</option>
                <option value="101-250">101-250 person</option>
                <option value="251-above">Above 251 person</option>
              </select>
            <p class="help-block">Total Person</p>
          </div>
          <div class="form-group col-lg-3">
            <label for=""></label>
            <button type="submit" class="btn btn-primary form-control" name="search">Search</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
@include('halls.footer')
