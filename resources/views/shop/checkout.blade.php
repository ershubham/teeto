@include('shop.header')
<div class="container">
  <div class="row">
    <form action="{{url('user/generateOrder')}}" method="post">
{{ csrf_field()}}

    <div class="col-lg-12">
      <h4>Checkout</h4>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <table class="table table-striped table-hover table-bordered">
        <tbody>
            <tr>
                <th>Item</th>
                <th>Unit Price</th>
                <th>QTY</th>
                <th>Total Price</th>
            </tr>
            <?php
              $tp=0;
            ?>
            @foreach($cartitem as $items)
            <tr>
                <td>{{$items->name}}<br /> Unit:{{$items->qty}}</td>
                <td>Rs.{{$items->sell_price}}</td>
                <td><span class = "badge">{{$items->qty}}</span></td>
                <td>Rs.{{$items->price}}</td>
            </tr>
          <?php
          $tp=$tp+$items->price;
           ?>
            @endforeach
            <tr>
                <th colspan="3"><span class="pull-right">Total Payable</span></th>
                <th>Rs <?php
                  echo $tp;
                 ?></th>
            </tr>
            <tr>
                <td></td>
                <td colspan="3"><a href="{{url('/user/cart')}}" class="pull-right btn btn-success">Edit Cart</a></td>
            </tr>
            <input type="hidden" name="total_amount" value="{{$tp}}" />
        </tbody>
    </table>
    </div>
    <div class="col-lg-4">
      <h4>Select Shipping Address</h4>
    </div>
    <div class="col-lg-4 col-lg-offset-4">
      <p class="text-right">
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#ship">Add Shipping Details</button>
      </p>
    </div>
    @foreach($ship as $ship)
    <div class="col-lg-3 well" id="address" style="margin-bottom:30px;">
              <label>
                  <input type="radio" name="address" value="{{$ship->id}}" required="required">
                  {{$ship->contact_name}},<br />{{$ship->contact_number}},
                  <br>{{$ship->address}},{{$ship->landmark}}
                  <br>{{$ship->city}},
                  <br />{{$ship->state}}-{{$ship->pincode}}
                  </label>
              </div>
    @endforeach
  </div>
  <div class="row">
    <div class="col-lg-4">
      <h5>Payment Mode</h5>
          <label>
          <input type="radio" name="payment" checked="true"  value="cod" />
          COD<br />
        </label>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-6" style="margin-bottom:90px;">
      <button type="submit" class="btn btn-primary form-control">Place order</button>
    </div>
  </div>
</div>
  </form>
<div id="ship" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Shipping Address</h4>
            </div>
            <form action="{{url('user/save_address')}}" method="post">
                {{ csrf_field()}}
                <div class="modal-body">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="ship_name" class="form-control" required="required">
                    </div>
                    <div class="form-group">
                        <label>Contact No</label>
                        <input type="text" name="con_number" class="form-control" required="required">
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <textarea name="address" class="form-control" required="required"></textarea>
                    </div>
                    <div class="form-group">
                        <label>City</label>
                        <input  type="text" name="city"  class="form-control" required="required">
                    </div>
                    <div class="form-group">
                        <label>Landmark</label>
                        <input  type="text" name="landmark" class="form-control" required="required">
                    </div>
                    <div class="form-group">
                        <label>State</label>
                        <input  type="text" name="state" class="form-control" required="required">
                    </div>
                    <div class="form-group">
                        <label>Pin Code</label>
                        <input  type="text" name="pincode" class="form-control" required="required">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="addAdd" class="btn btn-default" >Add Now</button>
                    <button type="button" name="addAdd" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@include('shop.footer')
