@include('shop.header')
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="text-warning">My Shopping Bag</h1>
      @if ($message = Session::get('msg'))
        <div class="alert alert-danger" role="alert">
          {{ Session::get('msg') }}
        </div>
      @endif
    </div>
    <div class="col-lg-12">
      @if(count($cartitem)!="")
      <table class="table table-striped table-hover table-bordered">
        <tbody>
            <tr>
                <th>Item</th>
                <th>Unit Price</th>
                <th>QTY</th>
                <th>Total Price</th>
            </tr>
            <?php
              $tp=0;
            ?>
            @foreach($cartitem as $items)
            <tr>
                <td>{{$items->name}}<br /> Unit:{{$items->qty}}</td>
                <td>{{$items->sell_price}}</td>
                <td>
                  <a href="{{route('shop.addtocart', ['id' => $items->pid])}}">
                    <i class="fa fa-caret-square-o-up"></i></a>
                    <span class = "badge">{{$items->qty}}</span>
                    <a href="{{route('shop.delfromcart', ['id' => $items->pid])}}">
                      <i class="fa fa-caret-square-o-down"></i></a>
                    </td>
                <td>{{$items->price}}</td>

            </tr>

          <?php
          $tp=$tp+$items->price;
           ?>
            @endforeach
            <tr>
                <th colspan="3"><span class="pull-right">Total Payable</span></th>
                <th> <?php
                  echo $tp;
                 ?></th>
            </tr>
            <tr>
                <td><a href="#" class="btn btn-primary">Continue Shopping</a></td>
                <td colspan="3"><a href="{{url('/user/checkout')}}" class="pull-right btn btn-success">Checkout</a></td>
            </tr>
        </tbody>
    </table>
      @else
      <h3 class="text-center">Your cart is empty</h3>
      @endif
    </div>
  </div>
</div>
@include('shop.footer')
