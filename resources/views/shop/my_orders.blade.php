@include('shop.header')
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          MY Orders
        </div>
        <div class="panel-body">
          <table class="table">
            <thead>
              <tr>
                <th>Order ID</th>
                <th>Order Amount</th>
                <th>Payment Mode</th>
                <th>Payment Status</th>
                <th>Order Status</th>
                <th>Order Date</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
                @foreach($orderlist as $orderlist)
                <tr>
                  <td>{{$orderlist->id}}</td>
                  <td>{{$orderlist->total_amount}}</td>
                  <td>{{$orderlist->payment_mode}}</td>
                  <td>{{$orderlist->payment_status}}</td>
                  <td>{{$orderlist->order_status}}</td>
                  <td>{{$orderlist->created_at}}</td>
                  <td><a href="/user/order_details/{{$orderlist->id}}" class="btn btn-primary">View Details</a></td>
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@include('shop.footer')
