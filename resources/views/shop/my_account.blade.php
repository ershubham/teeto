@include('shop.header')
<div class="container">
  <div class="row">
    <div class="col-lg-4">
      <div class="panel panel-primary">
        <div class="panel-heading">
          My Profile
        </div>
        <form action="{{url('/user/save_edit_profile')}}" method="post">
        <div class="panel-body">
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name">Name</label>

                    <input type="hidden" id="_token" name="_token" value="{{Session::token()}}">
                  <input id="name" type="text" class="form-control" name="name" value="{{$userdata->name}}" required autofocus>

                      @if ($errors->has('name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('name') }}</strong>
                          </span>
                      @endif
                </div>

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email">E-Mail Address</label>


                      <input id="email" type="email" readonly="true" class="form-control" name="email" value="{{$userdata->email}}" required>

                      @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif

              </div>
              <div class="form-group">
                <label>Phone No</label>

                  <input type="text" id="mobile" readonly="true" value="{{$userdata->contact_no}}" class="form-control" name="mobile">


              </div>


            </div>
            </div>
            <button class="btn btn-primary form-control" type="submit" name="update">Update Profile</button>
          </div>

        </form>

        </div>
      </div>
    </div>
  </div>
</div>
@include('shop.footer')
<script>
window.onload=function()
{
  $('#datepicker1').datepicker({
    format: 'dd-mm-yyyy',
    autoclose:true
  });
  $('#regsForm').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
      e.preventDefault();
      return false;
    }
  });
  $('#state').on('change',function(){
    var state=(this).value;
    if(state!="")
    {
      $.get("/getCity",{'state':state},function(data){
        $('#city').html(data);
      });
    }
  });
}


</script>
