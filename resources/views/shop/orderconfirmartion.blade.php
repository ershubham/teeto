@include('shop.header')
<div class="container">
    <div class="row">
        <dic class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="col-lg-12">
                        <p class="text-center">
                            <h1 class="text-center"><i class="fa fa-check-circle fa-3x" style="color:green;"></i><br> Your order is {{Session::get('status')}} Placed</h1>
                            @if(Session::get('status')=="successfully")
                            <h3 class="text-center">Your Order id is :   {{ Session::get('order_id') }}</h3>
                            @endif
                        <br>
                            <h5 class="text-center"><a href="{{url('/user/my_orders')}}"><button type="button" class="btn btn-lg btn-primary">View Your Order</button> </a></h5>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('shop.footer')
