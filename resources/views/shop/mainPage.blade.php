@include('shop.header')
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          @if ($message = Session::get('msg'))
            <div class="alert alert-danger" role="alert">
              {{ Session::get('msg') }} <a href="{{url('/shop/cart')}}" class="btn btn-success">View Cart</a>
            </div>
          @endif
          @foreach($products as $product)
          <div class="col-sm-4">
                <article class="col-item">
                  <div class="photo">
                    <a href="{{route('shop.addtocart', ['id' => $product->id])}}"> <img src="/public/{{$product->image}}" class="img-responsive" alt="Product Image" /> </a>
                  </div>
                  <div class="info">
                    <div class="row">
                      <div class="price-details col-md-6">
                        <h1>{{$product->name}}</h1><br />
                        <p class="details">
                          Product MRP: USD.{{$product->sell_price}}
                        </p>
                        <!-- <span class="price-new">Selling Price:Rs.{{$product->total_price}}</span> -->
                      </div>
                    </div>
                    <div class="separator clear-left">
                      <a href="{{route('shop.addtocart', ['id' => $product->id])}}" class="add-to-cart btn btn-default">add to cart</a>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </article>
              </div>
          @endforeach
        </div>
      </div>
    </div>
@include('shop.footer')
