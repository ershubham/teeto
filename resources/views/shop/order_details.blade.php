@include('shop.header')
<div class="container" id="detail">
  <div class="row">
    <div class="col-lg-12">
      <h3>Order Details</h3>
      <hr />
    </div>
    <div class="col-lg-3">
      <div class="panel panel-warning">
            <div class="panel-body">
                        <p>
                            <b>Order ID: {{$order->id}}</br></b>
                            <b>Order Date:</b>{{$order->created_at}}</br>
                            <b>Total Amount :</b>{{$order->total_amount}}</br>
                            <b>Payment By :</b> {{$order->payment_mode}}</br>
                            <b>order Status :</b> {{$order->order_status}}</br>
                        </p>
              </div>
        </div>
    </div>
    <div class="col-lg-3">
                <div class="panel panel-success">
                    <div class="panel-body">
                        <p>
                            <b>Shipping Address:</b><br>
                            {{$ship->contact_name}},<br />{{$ship->contact_number}},
                            <br>{{$ship->address}},{{$ship->landmark}}
                            <br>{{$ship->city}},
                            <br />{{$ship->state}}-{{$ship->pincode}}
                        </p>
                    </div>
                </div>
            </div>

  <div class="col-lg-6">
    <div class="panel panel-info">
                        <div class="panel-body table-responsive">
                                <table class="table table-responsive">
                                <tr>
                                    <td>Order Status</td>
                                    <td>Order Last Updated</td>
                                </tr>
                                <tr>
                                    <td>{{$order->order_status}}</td>
                                    <td>{{$order->updated_at}}</td>
                                </tr>
                                <tr>
                                   <td colspan="2" class="table-responsive">
                                       <b>Status Update</b><br>
                                      {{$order->order_details}}
                                   </td>
                                </tr>
                            </table>

                        </div>
                    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
                <h4>Product Details</h4>
                <hr>
            </div>
            <div class="col-lg-12">
                <table class="table table-bordered table-responsive cart_summary">
                    <thead>
                    <tr>
                        <th>Product</th>
                        <th>Unit Price + Shipping Cost</th>
                        <th>Qty</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($order_details as $od)
                        <tr>
                          <td>#{{$od->product_id}}::{{$od->name}}<br />{{$od->qty}}</td>
                          <td>{{$od->sell_price}} </td>
                          <td>{{$od->qty}}</td>
                          <td>{{$od->cost}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <div class="col-lg-12">
               <div class="col-lg-4">

               </div>
               <div class="col-lg-offset-4 col-lg-4 ">
                   <button class="btn btn-lg btn-primary" onclick="
                   var mywindow = window.open('', 'my div', 'height=auto,width=auto');
                   mywindow.document.write('<html><head><title>Print Invoice</title>');
                               mywindow.document.write('</head><body >');
                               mywindow.document.write(document.getElementById('detail').innerHTML);
                               mywindow.document.write('</body></html>');
                               mywindow.print();
                               mywindow.close();
                   ">Get Invoice</button>
               </div>
           </div>
       </div>
</div>
</div>
@include('shop.footer')
