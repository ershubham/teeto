@include('admin.header')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">

    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">All Orders</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-lg-12">
        @if ($message = Session::get('msg'))
          <div class="alert alert-danger" role="alert">
            {{ Session::get('msg') }}
          </div>
        @endif
        <h3>Order Details</h3>
        <hr />
      </div>
      <div class="col-lg-3">
        <div class="panel panel-warning">
              <div class="panel-body">
                          <p>
                              <b>Order ID: {{$order->id}}</br></b>
                              <b>Order Date:</b>{{$order->created_at}}</br>
                              <b>Total Amount :</b>{{$order->total_amount}}</br>
                              <b>Payment By :</b> {{$order->payment_mode}}</br>
                              <b>order Status :</b> {{$order->order_status}}</br>
                          </p>
                </div>
          </div>
      </div>
      <div class="col-lg-3">
                  <div class="panel panel-success">
                      <div class="panel-body">
                          <p>
                              <b>Shipping Address:</b><br>
                              {{$ship->contact_name}},<br />{{$ship->contact_number}},
                              <br>{{$ship->address}},{{$ship->landmark}}
                              <br>{{$ship->city}},
                              <br />{{$ship->state}}-{{$ship->pincode}}
                          </p>
                      </div>
                  </div>
              </div>

    <div class="col-lg-6">
      <div class="panel panel-info">
                          <div class="panel-body table-responsive">
                                  <table class="table table-responsive">
                                  <tr>
                                      <td>Order Status</td>
                                      <td>Order Last Updated</td>
                                  </tr>
                                  <tr>
                                      <td>{{$order->order_status}}</td>
                                      <td>{{$order->updated_at}}</td>
                                  </tr>
                                  <tr>
                                     <td colspan="2" class="table-responsive">
                                         <b>Status Update</b><br>
                                        {!!$order->order_details!!}
                                     </td>
                                  </tr>
                              </table>

                          </div>
                      </div>
    </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
                  <h4>Product Details</h4>
                  <hr>
              </div>
              <div class="col-lg-12">
                  <table class="table table-bordered table-responsive cart_summary">
                      <thead>
                      <tr>
                          <th>Product</th>
                          <th>Unit Price + Shipping Cost</th>
                          <th>Qty</th>
                          <th>Total</th>
                      </tr>
                      </thead>
                      <tbody>
                        @foreach($order_details as $od)
                          <tr>
                            <td>#{{$od->product_id}}::{{$od->name}}<br />{{$od->qty}}</td>
                            <td>Rs.{{$od->sell_price}}</td>
                            <td>{{$od->qty}}</td>
                            <td>{{$od->cost}}</td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                  <div class="col-lg-12">
                 <div class="col-lg-4">
                   <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#myModal">
  Update Order
 </button>
                 </div>
                 <div class="col-lg-offset-4 col-lg-4 ">
                     <button class="btn  btn-primary" onclick="
                     var mywindow = window.open('', 'my div', 'height=auto,width=auto');
                     mywindow.document.write('<html><head><title>Print Invoice</title>');
                                 mywindow.document.write('</head><body >');
                                 mywindow.document.write(document.getElementById('detail').innerHTML);
                                 mywindow.document.write('</body></html>');
                                 mywindow.print();
                                 mywindow.close();
                     ">Get Invoice</button>
                 </div>
             </div>
         </div>
    </div>
  </section>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Update Order Status</h4>
      </div>
      <form action="{{url('admin/updateorder')}}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="order_id" value="{{$order->id}}" />
      <div class="modal-body">

          <div class="form-group">
                                       <label>Status</label>
                                       <select class="form-control" name="status">
                                           <option value="{{$order->order_status}}">{{$order->order_status}}</option>
                                           <option value="processing">processing</option>
                                           <option value="packed">packed</option>
                                           <option value="shipped">shipped</option>
                                           <option value="delivered">delivered</option>
                                           <option value="pending">pending</option>
                                           <option value="cancelled">cancelled</option>
                                       </select>
                                   </div>
                                   <div class="form-group">
                                        <label>Description</label>
                                            <textarea name="des" id="des" class="form-control">
                                              {{$order->order_details}}
                                            </textarea>
                                    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- /.content-wrapper -->
<script>
     window.onload= function () {
        $('#des').summernote();
     }
 </script>
@include('admin.footer')
