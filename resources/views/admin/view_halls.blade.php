@include('admin.header')
<div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3>All Halls</h3>
      </div>
      <div class="col-lg-12">
        <table class="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Venue</th>
              <th>Price</th>
              <th>capacity</th>
            </tr>
          </thead>
          <tbody>
            @foreach($hall as $hr)
            <tr>
              <td>{{$hr->id}}</td>
              <td>{{$hr->name}}</td>
              <td>{{$hr->venue}}</td>
              <td>{{$hr->price_range}}</td>
              <td>{{$hr->capacity}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
</div>
@include('admin.footer')
