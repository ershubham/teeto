@include('admin.header')
<div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3>All Request for Hall Booking</h3>
      </div>
      <div class="col-lg-12">
        <table class="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Cust Name</th>
              <th>Cust Email</th>
              <th>Cust Contact Name</th>
              <th>Cust Contact Address</th>
              <th>Hall ID</th>
              <th>From Date</th>
              <th>To Date</th>
            </tr>
          </thead>
          <tbody>
            @foreach($hall_request as $hr)
            <tr>
              <td>{{$hr->id}}</td>
              <td>{{$hr->user_full_name}}</td>
              <td>{{$hr->user_email}}</td>
              <td>{{$hr->user_contact_no}}</td>
              <td>{{$hr->user_address}}</td>
              <td>{{$hr->hall_id}}</td>
              <td>{{$hr->from_date}}</td>
              <td>{{$hr->to_date}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
</div>
@include('admin.footer')
