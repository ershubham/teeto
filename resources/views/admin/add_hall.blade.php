@include('admin.header')
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <h2>Add Hall</h2>
    </div>
    <div class="col-lg-4">
      <form class="" action="/admin/save_hall" method="post">
          {{ csrf_field() }}
        <div class="form-group">
          <label for="">Name</label>
          <input type="text" name="hall_name" class="form-control" id="" placeholder="">
          <!-- <p class="help-block">Help text here.</p> -->
        </div>
        <div class="form-group">
          <label for="">Venue</label>
          <input type="text" name="venue" class="form-control" id="" placeholder="">
          <!-- <p class="help-block">Help text here.</p> -->
        </div>
        <div class="form-group">
          <label for="">Price Range</label>
          <input type="text" name="pricerange" class="form-control" id="" placeholder="">
          <!-- <p class="help-block">Help text here.</p> -->
        </div>
        <div class="form-group">
          <label for="">Suitable for</label>
          <input type="text" name="suitable" class="form-control" id="" placeholder="">
          <!-- <p class="help-block">Help text here.</p> -->
        </div>
        <div class="form-group">
          <label for="">Capacity</label>
            <select class="form-control" name="capacity">
              <option value="">Select Capacity</option>
              <option value="0-50">0-50 person</option>
              <option value="51-100">51-100 person</option>
              <option value="101-250">101-250 person</option>
              <option value="251-above">Above 251 person</option>
            </select>
        </div>
        <div class="form-group">
          <label for=""></label>
          <input type="submit" class="form-control btn btn-primary" name="go" value="submit" id="" placeholder="">
          <!-- <p class="help-block">Help text here.</p> -->
        </div>
      </form>
    </div>
  </div>
</div>
@include('admin.footer')
