@include('admin.header')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Product
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Add Product</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-lg-11">
        @if ($message = Session::get('msg'))
          <div class="alert alert-danger" role="alert">
            {{ Session::get('msg') }}
          </div>
        @endif
        <form action="{{url('/admin/save_product')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
          <div class="col-lg-4">

            <div class="form-group">
              <label>Product Name</label>
              <input type="text" class="form-control" name="p_name" required="required" />
            </div>
            <div class="form-group">
              <label>Product Selling Price</label>
              <input type="number" step="any" class="form-control" name="p_selling" required="required" />
            </div>
          </div>

          <div class="col-lg-8">
            <div class="form-group">
              <label>Image</label>
              <input type="file" name="p_image" class="form-control" required="required" />
            </div>
          <div class="form-group">
            <label>products description</label>
            <textarea class="form-control" id="p_des" name="p_des" required="required"></textarea>
          </div>
            <div class="form-group">
            <label>All Fields are required</label>
            <button type="submit" class="form-control btn btn-primary">Add Now</button>
          </div>
          </div>

        </form>
      </div>
    </div>
  </section>
</div>
<!-- /.content-wrapper -->
<script>
     window.onload= function () {
        $('#p_des').summernote();
     }
 </script>
@include('admin.footer')
