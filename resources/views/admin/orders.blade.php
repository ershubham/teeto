@include('admin.header')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      All Orders
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">All Orders</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h5>New Orders</h5>
          </div>
          <div class="panel-body">
            <table class="table">
              <thead>
                <tr>
                  <th>Order ID</th>
                  <th>User ID:: NAME</th>
                  <th>Order Amount</th>
                  <th>Payment Mode</th>
                  <th>Payment Status</th>
                  <th>Order Status</th>
                  <th>Order Date</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                  @foreach($orderlist as $orderlist)
                  <tr>
                    <td>{{$orderlist->id}}</td>
                    <td>{{$orderlist->user_id}}::{{$orderlist->name}}</td>
                    <td>{{$orderlist->total_amount}}</td>
                    <td>{{$orderlist->payment_mode}}</td>
                    <td>{{$orderlist->payment_status}}</td>
                    <td>{{$orderlist->order_status}}</td>
                    <td>{{$orderlist->created_at}}</td>
                    <td><a href="/admin/order_details/{{$orderlist->id}}" class="btn btn-primary">View Details</a></td>
                  </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!-- /.content-wrapper -->
@include('admin.footer')
