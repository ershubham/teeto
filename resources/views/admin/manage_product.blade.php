@include('admin.header')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage Product
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Manage Product</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-lg-12 well" style="overflow:auto">
        @if ($message = Session::get('msg'))
          <div class="alert alert-danger" role="alert">
            {{ Session::get('msg') }}
          </div>
        @endif
        <table id="my" class="table">
          <thead>
            <tr>
              <th>Product ID</th>
              <th>Name</th>
              <th>image</th>
              <th>description</th>


              <th>Selling</th>

              <th></th>
            </tr>
          </thead>
          <tbody>
            @foreach($pro as $pro)
            <tr>
              <td>{{$pro->id}}</td>
              <td>{{$pro->name}}</td>
              <td><img src="/public/{{$pro->image}}" height="80" /></td>
              <td>{!!$pro->description!!}</td>

              <td>{{$pro->sell_price}}</td>


              <td><a href="{{route('admin.delPro', ['id' => $pro->id])}}">Delete</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>
<!-- /.content-wrapper -->
<script>
  window.onload=function(){
    $("#my").DataTable();
  }
</script>
@include('admin.footer')
