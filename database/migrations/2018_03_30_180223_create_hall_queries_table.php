<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHallQueriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hall_queries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hall_id');
            $table->string('from_date');
            $table->string('to_date');
            $table->string('user_full_name');
            $table->string('user_email');
            $table->string('user_contact_no');
            $table->string('user_address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hall_queries');
    }
}
