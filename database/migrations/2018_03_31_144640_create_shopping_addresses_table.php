<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_addresses', function (Blueprint $table) {
          $table->increments('id');
          $table->string('user_id');
          $table->string('contact_name');
          $table->string('contact_number');
          $table->string('address');
          $table->string('city');
          $table->string('state');
          $table->string('landmark');
          $table->string('pincode');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_addresses');
    }
}
